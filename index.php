<?php  
    // Pemanggilan Class
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    // Inisialisasi Object
    $sheep = new Animal("shaun");
    $kodok = new Frog("buduk");
    $sungokong = new Ape("kera sakti");

    // menampilkan Data
    // Class Animal
    echo "Name : $sheep->name <br>";
    echo "Legs : $sheep->legs <br>";
    echo "Cold Blooded : $sheep->cold_blooded <br><br>";

    // Class Frog
    echo "Name : $kodok->name <br>";
    echo "Legs : $kodok->legs <br>";
    echo "Cold Blooded : $kodok->cold_blooded <br>";
    $kodok->jump();
    echo "<br><br>";

    // Class Ape
    echo "Name : $sungokong->name <br>";
    echo "Legs : $sungokong->legs <br>";
    echo "Cold Blooded : $sungokong->cold_blooded <br>";
    $sungokong->yell();
    echo "<br><br>";
?>

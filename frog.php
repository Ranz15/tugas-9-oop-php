<?php  
    // Pemanggilan Class
    require_once('animal.php');

    // Inheritance dari class animal
    class Frog extends Animal {
        public function jump(){
            echo "Jump : Hop Hop";
        }
    }
?>
<?php  
    // Pemanggilan Class
    require_once('animal.php');

    // Inheritance dari class animal
    class Ape extends Animal {
        public $legs = 2;

        public function yell(){
            echo "Yell : Auooo";
        }
    }


?>